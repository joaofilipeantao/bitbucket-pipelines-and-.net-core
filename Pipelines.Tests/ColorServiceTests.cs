using Xunit;

namespace Pipelines.Tests
{
    public class ColorServiceTests
    {
        private readonly IColorService _target;

        public ColorServiceTests()
        {
            _target = new ColorService();
        }
        
        [Fact]
        public void Red_Should_Return_Red()
        {
            Assert.Equal("Red", _target.Red());
        }

        [Fact]
        public void Green_Should_Return_Green()
        {
            Assert.Equal("Green", _target.Green());
        }

        [Fact]
        public void Blue_Should_Return_Blue()
        {
            Assert.Equal("Blue", _target.Blue());
        }
    }
}
